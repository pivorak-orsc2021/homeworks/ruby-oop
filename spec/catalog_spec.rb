require File.expand_path('../catalog.rb', __dir__)
require File.expand_path('../movie.rb', __dir__)
require File.expand_path('../music.rb', __dir__)
require File.expand_path('../item.rb', __dir__)

describe Catalog do
  let(:catalog_object) { described_class.new }
  let(:music) { Music.new(name: 'Some music', code: 1) }
  let(:movie) { Movie.new(name: 'Some movie', code: 2) }

  context 'when object initialized' do
    it 'has an empty list of items' do
      expect(catalog_object.items_list).to be_empty
    end
  end

  describe '#add' do
    let(:catalog_action) { catalog_object.add(music) }

    it 'adds items to the list' do
      expect { catalog_action }.to(
        change { catalog_object.items_list.length }.from(0).to(1)
      )
    end

    it 'contains last item music' do
      catalog_action

      expect(catalog_object.items_list.last).to eq(music)
    end

    it 'does not contain movie item' do
      catalog_action

      expect(catalog_object.items_list).not_to include(movie)
    end
  end

  describe '#remove' do
    let(:catalog_action) { catalog_object.remove(music.code) }

    before do
      catalog_object.add(music)
      catalog_object.add(movie)
    end

    it 'removes only 1 item' do
      expect { catalog_action }.to(
        change { catalog_object.items_list.length }.from(2).to(1)
      )
    end

    it 'removes item by its code' do
      catalog_action

      expect(catalog_object.items_list).not_to include(music)
    end

    it 'does not remove not needed items' do
      catalog_action

      expect(catalog_object.items_list).to include(movie)
    end
  end

  describe '#show' do
    it 'has show feature' do
      expect(catalog_object).to respond_to(:show)
    end
  end

  describe '#list' do
    it 'has list feature' do
      expect(catalog_object).to respond_to(:list)
    end
  end
end
