require File.expand_path('../catalog.rb', __dir__)
require File.expand_path('../movie.rb', __dir__)
require File.expand_path('../music.rb', __dir__)
require File.expand_path('../item.rb', __dir__)

describe Music do
  context 'when object created' do
    let(:options) do
      {
        name: 'Default name',
        code: 'Default code',
        size: 'Default size',
        singer: 'Default singer',
        duration: 12
      }
    end

    let(:music) { described_class.new(options) }

    it 'stores its name' do
      expect(music.name).to eq(options[:name])
    end

    it 'stores its code' do
      expect(music.code).to eq(options[:code])
    end

    it 'stores its size' do
      expect(music.size).to eq(options[:size])
    end

    it 'stores its singer' do
      expect(music.singer).to eq(options[:singer])
    end

    it 'stores its duration' do
      expect(music.duration).to eq(options[:duration])
    end

    context 'with category' do
      let(:category) { :music }

      it 'shows appropriate name' do
        expect(music.category).to eq(category)
      end
    end
  end
end
