require File.expand_path('../catalog.rb', __dir__)
require File.expand_path('../movie.rb', __dir__)
require File.expand_path('../music.rb', __dir__)
require File.expand_path('../item.rb', __dir__)

describe Movie do
  context 'when object created' do
    let(:options) do
      {
        name: 'Default name',
        code: 'Default code',
        size: 'Default size',
        director: 'Default director',
        main_actor: 'Default main actor',
        main_actress: 'Default main actress'
      }
    end

    let(:movie) { described_class.new(options) }

    it 'stores its name' do
      expect(movie.name).to eq(options[:name])
    end

    it 'stores its code' do
      expect(movie.code).to eq(options[:code])
    end

    it 'stores its size' do
      expect(movie.size).to eq(options[:size])
    end

    it 'stores its main_actor' do
      expect(movie.main_actor).to eq(options[:main_actor])
    end

    it 'stores its director' do
      expect(movie.director).to eq(options[:director])
    end

    it 'stores its main_actress' do
      expect(movie.main_actress).to eq(options[:main_actress])
    end

    context 'with category' do
      let(:category) { :movie }

      it 'shows appropriate name' do
        expect(movie.category).to eq(category)
      end
    end
  end
end
