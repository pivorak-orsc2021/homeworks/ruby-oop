require File.expand_path('../catalog.rb', __dir__)
require File.expand_path('../movie.rb', __dir__)
require File.expand_path('../music.rb', __dir__)
require File.expand_path('../item.rb', __dir__)

describe Item do
  context 'when object created' do
    let(:options) do
      {
        name: 'Default name',
        code: 'Default code',
        size: 'Default size'
      }
    end

    let(:item) { described_class.new(options) }

    it 'stores name' do
      expect(item.name).to eq(options[:name])
    end

    it 'stores code' do
      expect(item.code).to eq(options[:code])
    end

    it 'stores size' do
      expect(item.size).to eq(options[:size])
    end

    context 'when retrieves category' do
      it 'raises NotImplementedError' do
        expect { item.category }.to(
          raise_error(NotImplementedError, 'For subclasses only!')
        )
      end
    end
  end
end
